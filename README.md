Contributors: Shehzad Hathi (Roll No. 12666), CS653 Functional Programming, IITK
Email: shehzad@iitk.ac.in

"Combinatorial-curry" is an open-source library in Haskell in which I aim to implement a few basic algorithms related to combinatorics.

Presently, I have implemented several functions for integer partitions.

These include:
1) Calculating p(n) (the number of partitions of n) for all k <= n using Euler's formula
2) Listing P(n) (the partition of n)
3) Listing P(n) with exactly m parts
4) Finding parity (odd or even) of p(n)
5) Calculating number of plane partitions of n
6) Evaluating k coefficients of the gaussian polynomial (given n and m)
7) Calculating p(n) (the number of partitions of n) using Rademacher formula, which is imprecise as of now
For a detailed description, please visit http://shehzadhathi.weebly.com/uploads/1/1/0/0/110085789/combinatorial-curry.pdf


 