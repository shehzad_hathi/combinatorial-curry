module Partitions.Gaussian
( gaussian
) where

-----------------------------------------------------------
-- use of a functional pearl Power Series, Power Serious --
-----------------------------------------------------------
import Data.Ratio
infixl 7 .*
default (Integer, Rational, Double)

	-- constant series
ps0, x:: Num a => [a]
ps0 = 0 : ps0
x = 0 : 1 : ps0

	-- arithmetic
(.*):: Num a => a->[a]->[a]
c .* (f:fs) = c*f : c.*fs

instance Num a => Num [a] where
	negate (f:fs) = negate f : negate fs
	(f:fs) + (g:gs) = f+g : fs+gs
	(f:fs) * (g:gs) = f*g : (f.*gs + g.*fs + (0 : fs*gs))
	fromInteger c = fromInteger c : ps0

instance (Fractional a, Eq a) => Fractional [a] where
	recip fs = 1/fs
	(0:fs) / (0:gs) = fs/gs
	(f:fs) / (g:gs) = let q = f/g in
		q : (fs - q.*gs)/(g:gs)

	-- functional composition
compose:: (Num a, Eq a) => [a]->[a]->[a]
compose (f:fs) (0:gs) = f : gs*(compose fs (0:gs))

revert::(Fractional a, Eq a) => [a]->[a]
revert (0:fs) = rs where
	rs = 0 : 1/(compose fs rs)

	-- calculus
deriv:: Num a => [a]->[a]
deriv (f:fs) = (deriv1 fs 1) where
	deriv1 (g:gs) n = n*g : (deriv1 gs (n+1))

integral:: Fractional a => [a]->[a]
integral fs = 0 : (int1 fs 1) where
	int1 (g:gs) n = g/n : (int1 gs (n+1))

expx, cosx, sinx:: Fractional a => [a]
expx = 1 + (integral expx)
sinx = integral cosx
cosx = 1 - (integral sinx)

instance (Fractional a, Eq a) => Floating [a] where
	sqrt (0:0:fs) = 0 : sqrt fs
	sqrt (1:fs) = qs where
		qs = 1 + integral((deriv (1:fs))/(2.*qs))
-----------------------------------------------------------
-- the original code has been slightly modified --
-----------------------------------------------------------

--use of definition for evaluating gaussian polynomial
gaussian :: Int -> Int -> Int -> [Int]
gaussian n m k = map round $ take k $ gaussian' n m
gaussian' n m = q n / (q m*q (n-m))
	where
		q 1 = 1-x
		q n = q (n-1)*(1-x^n)