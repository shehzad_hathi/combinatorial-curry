module Partitions.Listing
( listAll --unchecked
, listM   --unchecked
) where

import Data.List
import Data.Maybe

-- function to list all integer partitions of a number --
listAll :: Int -> [[Int]]
{-
the algorithm used is based on the following idea (P(n) is the list of partitions for n):
P(n) : n, P(0)
	   n-1,P(1)
	in general
	   n-k,P(k) st each term <= (n-k)
-}

--memoized version to list partitions
listAll = (map listPart [0..] !!)

--unmemoized version to list partitions
listPart :: Int -> [[Int]]
listPart 0 = [[]]
listPart 1 = [[1]]
listPart n = concat $ zipWith (subListPart n) [1..n] (map listAll (reverse [0..(n-1)]))

--finds out the suitable partitions that should be included
subListPart :: Int->Int->[[Int]]->[[Int]]
subListPart n x xs | x >= (n-x) = map (x:) xs
				   | otherwise = map (x:) (filter check xs)
				   		where
							check = all (<=x)



--function to list all integer partitions of n with exactly m parts -- 
listM :: Int -> Int -> [[Int]]
{-
implementation of Hindenburg's algorithm
refer 14.2, Theory of Partitions by G.E.Andrews
-}

listM n m = unfoldr (constructnext n) seed
	where
		seed = (n-m+1):replicate (m-1) 1

constructnext :: Int -> [Int] -> Maybe([Int],[Int])
constructnext _ [] = Nothing
constructnext n (x:xs) = if isNothing index then Just(x:xs,[]) else Just(x:xs,next n ind (x:xs))
	where
		index = findIndex (\t -> x-t >= 2) xs
		Just ind = index

next :: Int -> Int -> [Int] -> [Int]
next n ind (x:xs) = (n-sum rest):rest
	where
		rest = replicate (ind+1) f' ++ s
		(f,s) = splitAt (ind+1) xs
		f' = last f+1