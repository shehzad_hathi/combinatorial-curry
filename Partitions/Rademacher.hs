module Partitions.Rademacher
( partitionR
) where

{-
the implementation used is from "Efficient implementation of the Hardy–Ramanujan–Rademacher formula" (http://arxiv.org/pdf/1205.5991.pdf)
and "Computations of the Partition Function" (http://www.jstor.org/stable/pdf/3618767.pdf?acceptTC=true)
the reason why this has been implemented along with Euler's formula is as given in "Computing the Integer Partition Function"
(http://www.math.clemson.edu/~kevja/PAPERS/ComputingPartitions-MathComp.pdf)
"Indeed, if one desires the value of p(n) for a single value of n then the exact formula of Rademacher yields a very fast algorithm. 
However, if one wishes to compute p(n) for all n ≤ N then Euler’s algorithm is much faster."

the other paper used in this implementation is "Computations of the Partition Function"
http://www.jstor.org/stable/3618767?seq=1#page_scan_tab_contents
-}


import Math.NumberTheory.Moduli
import Math.NumberTheory.Primes.Factorisation
import Control.Applicative
import Data.Maybe
import Data.List

-- ax is congruent to b modulo c
solveLinearModular :: Integer -> Integer -> Integer -> Maybe Integer
solveLinearModular a c b = (*b) <$> a'
						   		where
									a' = invertMod a c

-- (ax)^2 is congruent to b modulo c
solveQuadModular :: Integer -> Integer -> Integer -> Maybe Integer
solveQuadModular a c b = res >>= (solveLinearModular a c)
						 	where
								res = sqrtModF b c'
								c' = factorise' c


---------------------------------------------------------------------------------------------

--2.7
calcN12 :: Integer -> Integer -> Maybe Integer
calcN12 n k2 = solveLinearModular 1 2 (n-((k2*k2-1) `quot` 8))

calcN22 :: Integer -> Integer -> Maybe Integer
calcN22 n k2 = solveLinearModular 32 k2 (8*n+1)

--2.8
calcN14 :: Integer -> Integer -> Maybe Integer
calcN14 n k2 = solveLinearModular (k2*k2) 4 (n-2-((k2*k2-1) `quot` 8))

calcN24 :: Integer -> Integer -> Maybe Integer
calcN24 n k2 = solveLinearModular 128 k2 (8*n+5)

--2.9
calcN18 :: Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Maybe Integer
calcN18 n k1 k2 d1 d2 e = solveLinearModular (k2*k2*d2*e) k1 (d2*e*n+((k2*k2-1) `quot` d1))

calcN28 :: Integer -> Integer -> Integer -> Integer -> Integer -> Integer -> Maybe Integer
calcN28 n k1 k2 d1 d2 e = solveLinearModular (k1*k1*d1*e) k2 (d1*e*n+((k1*k1-1) `quot` d2))


---------------------------------------------------------------------------------------------

--2.3
calcM2 :: Integer -> Integer -> Maybe Integer
calcM2 v k = solveQuadModular 3 (8*k) v

--2.4
calcM3 :: Integer -> Integer -> Maybe Integer
calcM3 v k = solveQuadModular 8 (3*k) v

--2.5
calcMP :: Integer -> Integer -> Maybe Integer
calcMP v k = solveQuadModular 24 k v

---------------------------------------------------------------------------------------------

--2.6
calcAkn' :: Integer -> Integer -> (Integer,Int) -> Double
calcAkn' k v (p,l) = if rem v p == 0 then
					 	if l == 0 then
					 		(realToFrac (jacobi' 3 k))*((realToFrac k)**0.5)
					 	else
					 		0
					 else
					 	0

---------------------------------------------------------------------------------------------

--2.2
calcAkn :: Integer -> Integer -> (Integer,Int) -> Double
calcAkn n k (2,l) = if odd l then
						(-1)*(realToFrac (jacobi' (-1) m2))*(kf**0.5)*(sin (4*pi*m2f/(8*kf)))
					else 
						(realToFrac (jacobi' (-1) m2))*(kf**0.5)*(sin (4*pi*m2f/(8*kf)))
				where
					m2 = fromJust $ calcM2 v k
					m2f = realToFrac m2
					v = 1-24*n
					kf = realToFrac k

calcAkn n k (3,l) = if odd l then
						2*(realToFrac (jacobi' m3 3))*((kf/3)**0.5)*(sin (4*pi*m3f/(3*kf)))
					else
						(-2)*(realToFrac (jacobi' m3 3))*((kf/3)**0.5)*(sin (4*pi*m3f/(3*kf)))
				where
					m3 = fromJust $ calcM3 v k
					m3f = realToFrac m3	
					v = 1-24*n
					kf = realToFrac k	
						 		
calcAkn n k (p,l) = if (rawmp == Nothing) || (gcd v k /= 1) then
						calcAkn' k v (p,l)
					else
						2*(realToFrac (jacobi' 3 k))*(kf**0.5)*(cos (4*pi*(realToFrac $ fromJust rawmp)/kf))
				where
					rawmp = calcMP v k
					v = 1-24*n
					kf = realToFrac k
					

---------------------------------------------------------------------------------------------

--calculate list of (k1,k2)
kTupsGen :: [(Integer,Int)] -> Integer -> [(Integer,Integer)]
kTupsGen [_]    k = [(k,1)]
kTupsGen (x:xs) k = (t,k'):(kTupsGen xs k')
					where
						t = (fst x) ^ (snd x)
						k' = k `quot` t
					
--calculate list of (n1,n2)
nTupsGen :: [(Integer,Integer)] -> Integer -> [(Integer,Integer)]
nTupsGen [_]    n = [(n,1)] 
nTupsGen (x:xs) n = case k1 of 
						2 -> (fromJust $ calcN12 n k2, n2):(nTupsGen xs n2) --2.7
							where
								n2 = fromJust $ calcN22 n k2
						4 -> (fromJust $ calcN14 n k2, n2):(nTupsGen xs n2) --2.8
							where
								n2 = fromJust $ calcN24 n k2
						_ -> (fromJust $ calcN18 n k1 k2 d1 d2 e, n2):(nTupsGen xs n2) --2.9
							where
								n2 = fromJust $ calcN28 n k1 k2 d1 d2 e
								d1 = gcd 24 k1
								d2 = gcd 24 k2
								e = 24 `quot` (d1*d2)
					where
						k1 = fst x
						k2 = snd x


---------------------------------------------------------------------------------------------

--algorithm 2 in the paper
selberg :: Integer -> Integer -> Double
selberg k n = foldl (*) 1 (zipWith3 calcAkn (map fst nTups) (map fst kTups) primeFact)
	where
		primeFact = factorise' k
		kTups = kTupsGen primeFact k
		nTups = nTupsGen kTups n


---------------------------------------------------------------------------------------------

calcS :: Integer -> Integer -> Double
calcS k n = sum $ take (2*k') $ unfoldr cosines 0
	where
		cosines l = if (ms !! l) == 0 then
						Just (((-1)^l)*(cos ((pi*(6*(realToFrac l)+1))/(6*(realToFrac k)))),l+1)
					else
						Just (0,l+1)
		ms = mTups k n
		k' = fromIntegral k

rTups k = 2:(unfoldr rTupGen 2)
	where
		rTupGen r = Just (r',r')
			where
				r' = rem (r+3) k

mTups k n = m:(mTupGen rs k m)
	where
		m = rem n k
		rs = rTups k

mTupGen (r:rs) k m = m':(mTupGen rs k m')
	where
		m' = rem (m+r) k


-- algorithm 1 in the first paper
simpleSelberg :: Integer -> Integer -> Double
simpleSelberg k n | k <= 1 = k'
				  | k == 2 = (-1)^n
				  | otherwise = (sqrt (k' / 3))*calcS k n
				  	where
				  		k' = realToFrac k


---------------------------------------------------------------------------------------------

-- pages 48,49,50 in the the paper "Computations of the Partition Function"

v :: Integer -> Integer -> Double
v k n = (pi*pi*(24*n'-1))/(36*(k'^2))
	where
		n' = realToFrac n
		k' = realToFrac n

gv :: Double -> Double
gv v = ((v*(cosh v)) - (sinh v))/(2*(v^3))

---------------------------------------------------------------------------------------------

-- main function to implement Rademacher's exact formula for calculation of p(n) (the no. of partitions of n)
--partitionR :: Integer -> Integer
{-
partitionR n = round $ c*(sum seq)
	where
		c = (2*pi*pi)/(3*(sqrt 3))
		seq = map term [1..h]
			where
				h = max 10 $ round $ sqrt $ realToFrac n
				term k = ((simpleSelberg k n)*(gv $ v k n))/(k'**(2.5))
					where
						k' = realToFrac k
-}

---------------------------------------------------------------------------------------------
{-
This file includes various kinds of ways to implement certain functions.
Finally, what I have used is what has seemed to work for the final result.
However, some of the algorithms that I have not used but included in this code may be faster
than the existing algorithm.
-}
---------------------------------------------------------------------------------------------

--using the formula directly
partitionR :: Integer -> Integer
partitionR n = round $ sum $ take sqrtn $ map term [1..]
	where
		sqrtn = round $ sqrt n'
		n' = realToFrac n
		term k = const*(simpleSelberg k n)*u ((pi*sqrt (24*n'-1))/(6*k'))
			where
				k' = realToFrac k
				const = (sqrt (3/k'))*(4/(24*n'-1))
				u x = cosh x - (sinh x/x)


