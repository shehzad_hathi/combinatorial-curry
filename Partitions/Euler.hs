module Partitions.Euler
( partitions
) where

-- function to calculate list of number of partitions of all natural numbers less than or equal to the input
partitions :: Int -> [Integer]
--uses the inductive formula in terms of pentagonal numbers derived from Euler's identity

partitions n = take n (tail parts)
	where
		parts = map part [0..]
			where
				part 0 = 1
				part n = sum (signAlternate (map (parts !!) (pentanums n)))

--calculate the relevant pentagonal numbers (less than or equal to n)
pentanums :: Int -> [Int]
pentanums n =  takeWhile (>=0) (map ((n -) . pentagonals) [y | x <- [1 ..], y <- [-x, x]])

--calculate pentagonal numbers corresponding to each integer
pentagonals ::  Int -> Int
pentagonals j = (3*(j^2) + j) `quot` 2

--associate the appropriate sign with partition values as according to the formula
signAlternate :: [Integer] -> [Integer]
signAlternate xs = zipWith (*) xs (cycle [1,1,-1,-1])