module Partitions.Dimensions
( planePartitions --unchecked
) where

import Math.NumberTheory.Primes.Factorisation

planePart 0 = 1
planePart k = sum (zipWith (*) (map (sigma 2) $ reverse [1..k]) (map planePartitions [0..(k-1)])) `quot` k

-- memoized function to calculate the number of plane partitions of an integer
{-
Uses a recurrence relation given in 14.2, Theory of Partitions by G.E.Andrews
-}
planePartitions :: Integer -> Integer
planePartitions n = map planePart [0..] !! fromIntegral n