module Partitions.Parity
( parity --unchecked
) where

--function to find the parity of a partition function (odd or even) --
parity :: Int -> Int
{-
using a theorem due to Macmohan
refer Theorem 14.1, Theory of Partitions by G.E.Andrews
-}

parity n | res == 0 = mod (sum $ map parity' (arg0 n)) 2
		 | res == 1 = mod (sum $ map parity' (arg1 n)) 2
		 | res == 2 = mod (sum $ map parity' (arg2 n)) 2
		 | res == 3 = mod (sum $ map parity' (arg3 n)) 2
		 where
		 	res = n `mod` 4

parity' :: Int -> Int
parity' 0 = 1
parity' 1 = 1
parity' 2 = 0
parity' 3 = 1
parity' n = parity n

arg0 n = takeWhile (>=0) $ map (k-) $ concatMap (args 1) [0..]
	where
		k = n `quot` 4
arg1 n = takeWhile (>=0) $ map (k-) $ concatMap (args 3) [0..]
	where
		k = (n-1) `quot` 4
arg2 n = takeWhile (>=0) $ map (k-) $ concatMap (args 7) [0..]
	where
		k = (n-6) `quot` 4
arg3 n = takeWhile (>=0) $ map (k-) $ concatMap (args 5) [0..]
	where
		k = (n-3) `quot` 4

args :: Int -> Int -> [Int]
args _ 0 = [0]
args l i = [i*(8*i-l),i*(8*i+l)]