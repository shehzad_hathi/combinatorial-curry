/* DO NOT EDIT: This file is automatically generated by Cabal */

/* package arithmoi-0.4.1.3 */
#define VERSION_arithmoi "0.4.1.3"
#define MIN_VERSION_arithmoi(major1,major2,minor) (\
  (major1) <  0 || \
  (major1) == 0 && (major2) <  4 || \
  (major1) == 0 && (major2) == 4 && (minor) <= 1)

/* package base-4.6.0.1 */
#define VERSION_base "4.6.0.1"
#define MIN_VERSION_base(major1,major2,minor) (\
  (major1) <  4 || \
  (major1) == 4 && (major2) <  6 || \
  (major1) == 4 && (major2) == 6 && (minor) <= 0)

