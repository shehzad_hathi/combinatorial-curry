module Paths_combinatorial_curry (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,1,0,0], versionTags = []}
bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/home/shehzad/.cabal/bin"
libdir     = "/home/shehzad/.cabal/lib/combinatorial-curry-0.1.0.0/ghc-7.6.3"
datadir    = "/home/shehzad/.cabal/share/combinatorial-curry-0.1.0.0"
libexecdir = "/home/shehzad/.cabal/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catchIO (getEnv "combinatorial_curry_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "combinatorial_curry_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "combinatorial_curry_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "combinatorial_curry_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
